﻿using System;
using System.IO;
using atm;
using play;
using storage;

// random gaem second port (to C# this time)

/*
Notes:
 - Start porting the base game (+.)
 - Write a hud (++)
*/

namespace randomgaem_port
{
    class Program
    {
        private const string version = "rolling";

        static void Main(string[] args) { while (true) {
            Console.Clear();
            Console.Title = "Random Gaem -- Main menu";

            Console.WriteLine("\x1b[1mWelcome to Random Gaem! (shut up its gaem) \x1b[0m");
            Console.WriteLine("Random Gaem by Ezintegg2398");
            Console.WriteLine("Directed by jasonddebaba");
            Console.WriteLine("\n");
            Console.WriteLine("Version {0}", version);
            Console.WriteLine("1. Launch the ATM");
            Console.WriteLine("2. Exit");
            Console.Write("What to do?: ");
            string input = Console.ReadLine();
            ATM Atm = new ATM();
            if (input == "1")
            {
                Console.Clear();
                Atm.atmmenu();
            }
            else if (input == "2")
            {
                 Console.Clear();
                 Environment.Exit(0);
            }
        }}
    }
}

namespace play
{
    class Play
    {

    }
    class Player
    {

    }
}

namespace atm
{
    class ATM {

        wallet Wallet = new wallet();
        public string file = ".rgpin"; 
        public double storeBal;
        public string withdrawAmt;
        public string depositAmt;
        public string loginState = "not logged in";
        public string choicebf = null;

        void initATM() {
            Console.Title = "Random Gaem -- ATM";

            //Console.Clear();
            Console.WriteLine("\x1b[1mWelcome to the ATM!\x1b[0m");
        }
        public void deposit() {
            if (depositAmt != null) {
                storeBal = Wallet.deposit(Convert.ToDouble(depositAmt));   
            } else if (loginState == "not logged in") {
                Console.WriteLine("You will need to log in first.");
            }

            Console.Write("How much money to deposit?: ");
            depositAmt = Console.ReadLine();

            Wallet.deposit(Convert.ToDouble(depositAmt));
        }

        public void withdraw() {
            if (loginState == "not logged in") {
                Console.WriteLine("You will need to log in first.");
            }

            Console.Write("How much money to withdraw?");
            withdrawAmt = Console.ReadLine();

            Wallet.withdraw(Convert.ToDouble(withdrawAmt));
        }

        public void createPin(bool create) {
            try {
                if (create == true)
                {
                    if (File.Exists(this.file))
                    {
                        Console.WriteLine("You have already created the pin, you just need to log in");
                        return;
                    }
                } else if (create == false)
                {
                    File.Delete(file);
                }

                using FileStream writeStream = File.Create(this.file);
                using var pinWriter = new StreamWriter(writeStream);

                string pincode;
                string verifypincode;
                
                for (int i = 0; i < 3; i++)
                {
                    Console.Write("Enter a PIN to set: ");
                    pincode = Console.ReadLine();
                    Console.Write("Re-enter that PIN to verify: ");
                    verifypincode = Console.ReadLine();

                    if (pincode != verifypincode)
                    {
                        if (i == 3)
                        {
                            Console.WriteLine("You tried too many times but the PIN code you reentered was incorrect, please try again.");
                        }
                        Console.WriteLine("The PIN Codes do not match, Please try again.");
                    }
                    else
                    {
                        pinWriter.WriteLine(verifypincode);
                        return;
                    }
                }
                pinWriter.Close();
            } catch (FileNotFoundException) {
                Console.Clear();
                Console.WriteLine("An error occured, there was a FileNotFoundException.");
            } catch (System.IO.IOException)
            {
                Console.Clear();
                Console.WriteLine("ioexception occured, go report this issue");
            }

        }

        public void login() {
            try {
                
                string pinInput;
                string pinInputVerification;

                using FileStream readStream = File.OpenRead(this.file);
                using var pinReader = new StreamReader(readStream);

                pinInputVerification = pinReader.ReadLine();

                for (int i = 0; i < 3; i++) {
                    Console.Write("Enter your PIN: ");
                    pinInput = Console.ReadLine();

                    if (pinInput == pinInputVerification) {
                        Console.Clear();
                        Console.Write("Login Success!");
                        loginState = "logged in!";
                        return;
                    } else {
                        if (i == 3) {
                            Console.WriteLine("You have entered the pin wrong too many times, please try again later.");
                            return;
                        }
                        Console.WriteLine("You entered the wrong PIN, please try again");
                    }

                }
                pinReader.Close();
            } catch (FileNotFoundException) {
                Console.Clear();
                Console.WriteLine("you didn't set a pin yet. Please go set one from the main menu.");
                return;
            }
           
        }
        public void checkBal() {
            Wallet.checkbal();
        }

        public void deletePin()
        {
            // handle that
        }

        public void changePin() {
            if (loginState != "logged in!") {
                Console.Clear();
                Console.WriteLine("You didn't log in yet, please do so first.");
                return;
            } else if (!File.Exists(file)) {
                 Console.Clear();
                 Console.WriteLine("You didn't set a PIN yet, how can you change a nonexistent pin?");
            }


            using FileStream verify = File.OpenRead(file);
            using var read = new StreamReader(verify);
            string pin = read.ReadLine();
            read.Close();
            verify.Close();

            string changePinInput;

            for (int i = 0; i < 3; i++) {
                Console.Write("enter your current PIN: ");
                changePinInput = Console.ReadLine();

                if (changePinInput == pin) {
                    Console.WriteLine("Success, now you can change your PIN!");
                    this.createPin(false);
                    return;
                } else if (changePinInput != pin) {
                    if (i == 3) {
                        Console.Clear();
                        Console.WriteLine("You tried too many times, please try again from the main menu.");
                        return;
                    }
                    Console.Clear();
                    Console.WriteLine("PIN is wrong, please try again.");
                }
            }
        }
        public void atmmenu() { while (true) {
            this.initATM();
            string choiceBf;

            Console.WriteLine("\x1b[1mWhat to do?\x1b[0m");
            Console.WriteLine("1. Log in to the ATM [{0}]", this.loginState);
            Console.WriteLine("2. Create an ATM PIN");
            Console.WriteLine("3. Change your ATM PIN");
            Console.WriteLine("4. Deposit some money");
            Console.WriteLine("5. Change your ATM PIN");

            Console.Write("What to do?: ");
            choiceBf = Console.ReadLine();

            if (choiceBf == "1") {
                this.login();
            } else if (choiceBf == "2") {
                this.createPin(true);
            } else if (choiceBf == "3") {
                this.changePin();
            } else if (choiceBf == "4") {
                this.deposit();
            } else if (choiceBf == "5") {
                this.withdraw();
            } else if (choiceBf == "6") {
                return;
            }
        }}      
    }  

}

namespace storage
{  
    class wallet
    {
        public string writebf;
        public static string file = ".rgwallet";
        public string fileBf;

        public double deposit(double depositAmount) {
            fileBf = File.ReadAllText(file);
            double balance = depositAmount + Convert.ToDouble(fileBf);
            writebf = balance.ToString();
            File.WriteAllText(file, writebf);
            return balance;
        }
        public double withdraw(double withdrawAmount) {
            fileBf = File.ReadAllText(file);
            if (withdrawAmount > Convert.ToDouble(fileBf))
            {
                Console.WriteLine("You're too broke to withdraw that much./...");
                return Convert.ToDouble(fileBf);
            }
            double balance = Convert.ToDouble(fileBf) - withdrawAmount;
            File.WriteAllText(file, fileBf);
            return balance;
        }    
        public double checkbal() {
            fileBf = File.ReadAllText(file);
            return Convert.ToDouble(fileBf);
        }                    
    }
}